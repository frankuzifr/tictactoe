using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Canvases : MonoBehaviour
{
    public Canvas[] interfaces;
    public GameObject[] fons;
    void Start()
    {
        if (PlayerPrefs.GetInt("selected") == 0)
        {
            interfaces[0].gameObject.SetActive(true);
            interfaces[1].gameObject.SetActive(false);
            fons[0].SetActive(true);
            fons[1].SetActive(false);

        }
        else if (PlayerPrefs.GetInt("selected") == 1)
        {
            interfaces[0].gameObject.SetActive(false);
            interfaces[1].gameObject.SetActive(true);
            fons[0].SetActive(false);
            fons[1].SetActive(true);
        }

    }


}
