﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SPGame : MonoBehaviour
{
    public Button cell;
    public Button[] cells;
    public Sprite[] ticTacToe;
    private int turnComp, sim = 2;
    private int tagBn;
    public GameObject win, restartButton, winEffects, winAudio, loseAudio;
    public GameObject[] effectsToWin;
    public Text winText, turnText;
    public AudioSource pen;
    private bool end = false;

    public void CellsClick()
    {
        PlayerPrefs.SetInt("numOfClicks", PlayerPrefs.GetInt("numOfClicks") + 1);
        tagBn = Convert.ToInt32(cell.tag);
        PlayerPrefs.SetInt("cell" + tagBn, sim);
        cell.image.sprite = ticTacToe[sim];
        cell.interactable = false;
        pen.Play();
        if (PlayerPrefs.GetInt("numOfClicks") > 4)
        {
            Result();
        }
        if (PlayerPrefs.GetInt("numOfClicks") < 9 && !end)
        {
            CompTurn();
        }
    }


    private void CompTurn()
    {
        if ((PlayerPrefs.GetInt("cell" + 1) == 1 && PlayerPrefs.GetInt("cell" + 2) == 1 && PlayerPrefs.GetInt("cell" + 3) == 0) ||
            (PlayerPrefs.GetInt("cell" + 6) == 1 && PlayerPrefs.GetInt("cell" + 9) == 1 && PlayerPrefs.GetInt("cell" + 3) == 0) ||
            (PlayerPrefs.GetInt("cell" + 5) == 1 && PlayerPrefs.GetInt("cell" + 7) == 1 && PlayerPrefs.GetInt("cell" + 3) == 0))
        {
            turnComp = 3;
        }
        else if ((PlayerPrefs.GetInt("cell" + 1) == 1 && PlayerPrefs.GetInt("cell" + 3) == 1 && PlayerPrefs.GetInt("cell" + 2) == 0) ||
                 (PlayerPrefs.GetInt("cell" + 5) == 1 && PlayerPrefs.GetInt("cell" + 8) == 1 && PlayerPrefs.GetInt("cell" + 2) == 0))
        {
            turnComp = 2;
        }
        else if ((PlayerPrefs.GetInt("cell" + 2) == 1 && PlayerPrefs.GetInt("cell" + 3) == 1 && PlayerPrefs.GetInt("cell" + 1) == 0) ||
                 (PlayerPrefs.GetInt("cell" + 4) == 1 && PlayerPrefs.GetInt("cell" + 7) == 1 && PlayerPrefs.GetInt("cell" + 1) == 0) ||
                 (PlayerPrefs.GetInt("cell" + 5) == 1 && PlayerPrefs.GetInt("cell" + 9) == 1 && PlayerPrefs.GetInt("cell" + 1) == 0))
        {
            turnComp = 1;
        }
        else if ((PlayerPrefs.GetInt("cell" + 1) == 1 && PlayerPrefs.GetInt("cell" + 4) == 1 && PlayerPrefs.GetInt("cell" + 7) == 0) ||
                 (PlayerPrefs.GetInt("cell" + 3) == 1 && PlayerPrefs.GetInt("cell" + 5) == 1 && PlayerPrefs.GetInt("cell" + 7) == 0) ||
                 (PlayerPrefs.GetInt("cell" + 8) == 1 && PlayerPrefs.GetInt("cell" + 9) == 1 && PlayerPrefs.GetInt("cell" + 7) == 0))
        {
            turnComp = 7;
        }
        else if ((PlayerPrefs.GetInt("cell" + 1) == 1 && PlayerPrefs.GetInt("cell" + 7) == 1 && PlayerPrefs.GetInt("cell" + 4) == 0) ||
                 (PlayerPrefs.GetInt("cell" + 5) == 1 && PlayerPrefs.GetInt("cell" + 6) == 1 && PlayerPrefs.GetInt("cell" + 4) == 0))
        {
            turnComp = 4;
        }
        else if ((PlayerPrefs.GetInt("cell" + 1) == 1 && PlayerPrefs.GetInt("cell" + 5) == 1 && PlayerPrefs.GetInt("cell" + 9) == 0) ||
                 (PlayerPrefs.GetInt("cell" + 3) == 1 && PlayerPrefs.GetInt("cell" + 6) == 1 && PlayerPrefs.GetInt("cell" + 9) == 0) ||
                 (PlayerPrefs.GetInt("cell" + 7) == 1 && PlayerPrefs.GetInt("cell" + 8) == 1 && PlayerPrefs.GetInt("cell" + 9) == 0))
        {
            turnComp = 9;
        }
        else if ((PlayerPrefs.GetInt("cell" + 1) == 1 && PlayerPrefs.GetInt("cell" + 9) == 1 && PlayerPrefs.GetInt("cell" + 5) == 0) ||
                 (PlayerPrefs.GetInt("cell" + 2) == 1 && PlayerPrefs.GetInt("cell" + 8) == 1 && PlayerPrefs.GetInt("cell" + 5) == 0) ||
                 (PlayerPrefs.GetInt("cell" + 3) == 1 && PlayerPrefs.GetInt("cell" + 7) == 1 && PlayerPrefs.GetInt("cell" + 5) == 0) ||
                 (PlayerPrefs.GetInt("cell" + 4) == 1 && PlayerPrefs.GetInt("cell" + 6) == 1 && PlayerPrefs.GetInt("cell" + 5) == 0))
        {
            turnComp = 5;
        }
        else if ((PlayerPrefs.GetInt("cell" + 2) == 1 && PlayerPrefs.GetInt("cell" + 5) == 1 && PlayerPrefs.GetInt("cell" + 8) == 0) ||
                 (PlayerPrefs.GetInt("cell" + 7) == 1 && PlayerPrefs.GetInt("cell" + 9) == 1 && PlayerPrefs.GetInt("cell" + 8) == 0))
        {
            turnComp = 8;
        }
        else if ((PlayerPrefs.GetInt("cell" + 3) == 1 && PlayerPrefs.GetInt("cell" + 9) == 1 && PlayerPrefs.GetInt("cell" + 6) == 0) ||
                 (PlayerPrefs.GetInt("cell" + 4) == 1 && PlayerPrefs.GetInt("cell" + 5) == 1 && PlayerPrefs.GetInt("cell" + 6) == 0))
        {
            turnComp = 6;
        }
        else if ((PlayerPrefs.GetInt("cell" + 1) == 2 && PlayerPrefs.GetInt("cell" + 2) == 2 && PlayerPrefs.GetInt("cell" + 3) == 0) ||
                 (PlayerPrefs.GetInt("cell" + 6) == 2 && PlayerPrefs.GetInt("cell" + 9) == 2 && PlayerPrefs.GetInt("cell" + 3) == 0) ||
                 (PlayerPrefs.GetInt("cell" + 5) == 2 && PlayerPrefs.GetInt("cell" + 7) == 2 && PlayerPrefs.GetInt("cell" + 3) == 0))
        {
            turnComp = 3;
        }
        else if ((PlayerPrefs.GetInt("cell" + 1) == 2 && PlayerPrefs.GetInt("cell" + 3) == 2 && PlayerPrefs.GetInt("cell" + 2) == 0) ||
                 (PlayerPrefs.GetInt("cell" + 5) == 2 && PlayerPrefs.GetInt("cell" + 8) == 2 && PlayerPrefs.GetInt("cell" + 2) == 0))
        {
            turnComp = 2;
        }
        else if ((PlayerPrefs.GetInt("cell" + 2) == 2 && PlayerPrefs.GetInt("cell" + 3) == 2 && PlayerPrefs.GetInt("cell" + 1) == 0) ||
                 (PlayerPrefs.GetInt("cell" + 4) == 2 && PlayerPrefs.GetInt("cell" + 7) == 2 && PlayerPrefs.GetInt("cell" + 1) == 0) ||
                 (PlayerPrefs.GetInt("cell" + 5) == 2 && PlayerPrefs.GetInt("cell" + 9) == 2 && PlayerPrefs.GetInt("cell" + 1) == 0))
        {
            turnComp = 1;
        }
        else if ((PlayerPrefs.GetInt("cell" + 1) == 2 && PlayerPrefs.GetInt("cell" + 4) == 2 && PlayerPrefs.GetInt("cell" + 7) == 0) ||
                 (PlayerPrefs.GetInt("cell" + 3) == 2 && PlayerPrefs.GetInt("cell" + 5) == 2 && PlayerPrefs.GetInt("cell" + 7) == 0) ||
                 (PlayerPrefs.GetInt("cell" + 8) == 2 && PlayerPrefs.GetInt("cell" + 9) == 2 && PlayerPrefs.GetInt("cell" + 7) == 0))
        {
            turnComp = 7;
        }
        else if ((PlayerPrefs.GetInt("cell" + 1) == 2 && PlayerPrefs.GetInt("cell" + 7) == 2 && PlayerPrefs.GetInt("cell" + 4) == 0) ||
                 (PlayerPrefs.GetInt("cell" + 5) == 2 && PlayerPrefs.GetInt("cell" + 6) == 2 && PlayerPrefs.GetInt("cell" + 4) == 0))
        {
            turnComp = 4;
        }
        else if ((PlayerPrefs.GetInt("cell" + 1) == 2 && PlayerPrefs.GetInt("cell" + 5) == 2 && PlayerPrefs.GetInt("cell" + 9) == 0) ||
                 (PlayerPrefs.GetInt("cell" + 3) == 2 && PlayerPrefs.GetInt("cell" + 6) == 2 && PlayerPrefs.GetInt("cell" + 9) == 0) ||
                 (PlayerPrefs.GetInt("cell" + 7) == 2 && PlayerPrefs.GetInt("cell" + 8) == 2 && PlayerPrefs.GetInt("cell" + 9) == 0))
        {
            turnComp = 9;
        }
        else if ((PlayerPrefs.GetInt("cell" + 1) == 2 && PlayerPrefs.GetInt("cell" + 9) == 2 && PlayerPrefs.GetInt("cell" + 5) == 0) ||
                 (PlayerPrefs.GetInt("cell" + 2) == 2 && PlayerPrefs.GetInt("cell" + 8) == 2 && PlayerPrefs.GetInt("cell" + 5) == 0) ||
                 (PlayerPrefs.GetInt("cell" + 3) == 2 && PlayerPrefs.GetInt("cell" + 7) == 2 && PlayerPrefs.GetInt("cell" + 5) == 0) ||
                 (PlayerPrefs.GetInt("cell" + 4) == 2 && PlayerPrefs.GetInt("cell" + 6) == 2 && PlayerPrefs.GetInt("cell" + 5) == 0))
        {
            turnComp = 5;
        }
        else if ((PlayerPrefs.GetInt("cell" + 2) == 2 && PlayerPrefs.GetInt("cell" + 5) == 2 && PlayerPrefs.GetInt("cell" + 8) == 0) ||
                 (PlayerPrefs.GetInt("cell" + 7) == 2 && PlayerPrefs.GetInt("cell" + 9) == 2 && PlayerPrefs.GetInt("cell" + 8) == 0))
        {
            turnComp = 8;
        }
        else if ((PlayerPrefs.GetInt("cell" + 3) == 2 && PlayerPrefs.GetInt("cell" + 9) == 2 && PlayerPrefs.GetInt("cell" + 6) == 0) ||
                 (PlayerPrefs.GetInt("cell" + 4) == 2 && PlayerPrefs.GetInt("cell" + 5) == 2 && PlayerPrefs.GetInt("cell" + 6) == 0))
        {
            turnComp = 6;
        }
        else
        {
            turnComp = UnityEngine.Random.Range(1, 10);
            while (PlayerPrefs.GetInt("cell" + turnComp) != 0)
            {
                turnComp = UnityEngine.Random.Range(1, 10);
            }
        }
        
        PlayerPrefs.SetInt("numOfClicks", PlayerPrefs.GetInt("numOfClicks") + 1);
        foreach (Button but in cells)
        {
            if (but.tag == Convert.ToString(turnComp))
            {
                but.image.sprite = ticTacToe[1];
                but.interactable = false;
                PlayerPrefs.SetInt("cell" + turnComp, 1);
            }
        }
        

        if (PlayerPrefs.GetInt("numOfClicks") > 4)
        {
            Result();
        }
    }
    private void Result()
    {
        if ((PlayerPrefs.GetInt("cell" + 1) == 1 && PlayerPrefs.GetInt("cell" + 2) == 1 && PlayerPrefs.GetInt("cell" + 3) == 1) ||
            (PlayerPrefs.GetInt("cell" + 1) == 1 && PlayerPrefs.GetInt("cell" + 5) == 1 && PlayerPrefs.GetInt("cell" + 9) == 1) ||
            (PlayerPrefs.GetInt("cell" + 1) == 1 && PlayerPrefs.GetInt("cell" + 4) == 1 && PlayerPrefs.GetInt("cell" + 7) == 1) ||
            (PlayerPrefs.GetInt("cell" + 2) == 1 && PlayerPrefs.GetInt("cell" + 5) == 1 && PlayerPrefs.GetInt("cell" + 8) == 1) ||
            (PlayerPrefs.GetInt("cell" + 3) == 1 && PlayerPrefs.GetInt("cell" + 6) == 1 && PlayerPrefs.GetInt("cell" + 9) == 1) ||
            (PlayerPrefs.GetInt("cell" + 4) == 1 && PlayerPrefs.GetInt("cell" + 5) == 1 && PlayerPrefs.GetInt("cell" + 6) == 1) ||
            (PlayerPrefs.GetInt("cell" + 7) == 1 && PlayerPrefs.GetInt("cell" + 8) == 1 && PlayerPrefs.GetInt("cell" + 9) == 1) ||
            (PlayerPrefs.GetInt("cell" + 7) == 1 && PlayerPrefs.GetInt("cell" + 5) == 1 && PlayerPrefs.GetInt("cell" + 3) == 1))
        {
            turnText.gameObject.SetActive(false);
            win.SetActive(true);
            restartButton.SetActive(true);
            winText.text = "You lose";
            foreach (GameObject obj in effectsToWin)
            {
                obj.SetActive(false);
            }
            loseAudio.SetActive(true);
            end = true;
            PlayerPrefs.DeleteKey("numOfClicks");
            for (int i = 1; i < 10; i++)
            {
                PlayerPrefs.DeleteKey("cell" + i);
            }
        }
        else if ((PlayerPrefs.GetInt("cell" + 1) == 2 && PlayerPrefs.GetInt("cell" + 2) == 2 && PlayerPrefs.GetInt("cell" + 3) == 2) ||
                 (PlayerPrefs.GetInt("cell" + 1) == 2 && PlayerPrefs.GetInt("cell" + 5) == 2 && PlayerPrefs.GetInt("cell" + 9) == 2) ||
                 (PlayerPrefs.GetInt("cell" + 1) == 2 && PlayerPrefs.GetInt("cell" + 4) == 2 && PlayerPrefs.GetInt("cell" + 7) == 2) ||
                 (PlayerPrefs.GetInt("cell" + 2) == 2 && PlayerPrefs.GetInt("cell" + 5) == 2 && PlayerPrefs.GetInt("cell" + 8) == 2) ||
                 (PlayerPrefs.GetInt("cell" + 3) == 2 && PlayerPrefs.GetInt("cell" + 6) == 2 && PlayerPrefs.GetInt("cell" + 9) == 2) ||
                 (PlayerPrefs.GetInt("cell" + 4) == 2 && PlayerPrefs.GetInt("cell" + 5) == 2 && PlayerPrefs.GetInt("cell" + 6) == 2) ||
                 (PlayerPrefs.GetInt("cell" + 7) == 2 && PlayerPrefs.GetInt("cell" + 8) == 2 && PlayerPrefs.GetInt("cell" + 9) == 2) ||
                 (PlayerPrefs.GetInt("cell" + 7) == 2 && PlayerPrefs.GetInt("cell" + 5) == 2 && PlayerPrefs.GetInt("cell" + 3) == 2))
        {
            turnText.gameObject.SetActive(false);
            win.SetActive(true);
            restartButton.SetActive(true);
            winText.text = "You win";
            loseAudio.SetActive(false);
            end = true;
            PlayerPrefs.DeleteKey("numOfClicks");
            for (int i = 1; i < 10; i++)
            {
                PlayerPrefs.DeleteKey("cell" + i);
            }
        }
        else if (PlayerPrefs.GetInt("numOfClicks") == 9)
        {
            turnText.gameObject.SetActive(false);
            win.SetActive(true);
            restartButton.SetActive(true);
            winText.text = "Nobody won";
            winEffects.SetActive(false);
            foreach (GameObject obj in effectsToWin)
            {
                obj.SetActive(false);
            }
            end = true;
            PlayerPrefs.DeleteKey("numOfClicks");
            for (int i = 1; i < 10; i++)
            {
                PlayerPrefs.DeleteKey("cell" + i);
            }
        }
        else
        {
            return;
        }
    }

    private void Clearing()
    {
        PlayerPrefs.DeleteKey("numOfClicks");
        for (int i = 1; i < 10; i++)
        {
            PlayerPrefs.DeleteKey("cell" + i);
        }
    }
}
