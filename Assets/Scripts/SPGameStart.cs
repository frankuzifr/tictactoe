using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SPGameStart : MonoBehaviour
{
    public Button[] cells;
    public Sprite[] ticTacToe;
    private int sim, turnComp;
    public Text turnText;
    public AudioSource pen;
    public Canvas[] interfaces;
    private void Start()
    {
        
        StartCoroutine(Delay()); 
    }
    IEnumerator Delay()
    {
        PlayerPrefs.DeleteKey("numOfClicks");
        for (int i = 1; i < 10; i++)
        {
            PlayerPrefs.DeleteKey("cell" + i);
        }
        sim = UnityEngine.Random.Range(1, 3);
        foreach (Button but in cells)
        {
            but.interactable = false;
        }
        yield return new WaitForSeconds(1f);
        if (sim == 1)
        {
            PlayerPrefs.SetInt("numOfClicks", 1);
            turnComp = UnityEngine.Random.Range(1, 10);
            foreach (Button but in cells)
            {
                but.interactable = true;
                but.interactable = true;
                if (but.tag == Convert.ToString(turnComp))
                {
                    but.image.sprite = ticTacToe[1];
                    but.interactable = false;
                    PlayerPrefs.SetInt("cell" + turnComp, sim);
                }
            }
        }
        else
        {
            PlayerPrefs.SetInt("numOfClicks", 0);
            foreach (Button but in cells)
            {
                but.interactable = true;
            }
        }
        
    }

}
