using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Scroll : MonoBehaviour
{
    public GameObject[] canvas;
    private Vector2[] canvasPos;
    private Vector2 changePos;
    public Color fon;
    public Color[] fonColor;
    private RectTransform contentRect;
    public float changeSpeed;
    private int selectedCanvasID;

    private bool isScrolling;
    void Start()
    {
        fon = Camera.main.backgroundColor;
        contentRect = GetComponent<RectTransform>();
        canvasPos = new Vector2[canvas.Length];
        for (int i = 0; i < canvas.Length; i++)
        {
            canvasPos[i] = -canvas[i].transform.localPosition;
        }
    }

    private void FixedUpdate()
    {
        float nearestPos = float.MaxValue;
        for (int i = 0; i < canvas.Length; i++)
        {
            float distance = Mathf.Abs(contentRect.anchoredPosition.x - canvasPos[i].x);
            if (distance < nearestPos)
            {
                nearestPos = distance;
                selectedCanvasID = i;
            }
        }
        if (isScrolling)
        {
            return;
        }
        if (selectedCanvasID == 0)
        {
            fon = fonColor[0];
        }
        else if (selectedCanvasID == 1)
        {
            fon = fonColor[1];
        }
        if (Camera.main.backgroundColor != fon)
        {
            Camera.main.backgroundColor = Color.Lerp(Camera.main.backgroundColor, fon, 8 * Time.deltaTime);
        }
        changePos.x = Mathf.SmoothStep(contentRect.anchoredPosition.x, canvasPos[selectedCanvasID].x, changeSpeed * Time.deltaTime);
        contentRect.anchoredPosition = changePos;
    }
    
    public void Scrolling(bool scroll)
    {
        isScrolling = scroll;
    }
}
