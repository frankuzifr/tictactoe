﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnimationPole : MonoBehaviour
{
    public Image AnimPole;
    public float changePictures, penDraws;
    public AudioSource pen;
    public Sprite[] fon;
    private void Start()
    {
        StartCoroutine(AnimationForPole());
        StartCoroutine(PenAudio());
    }

    IEnumerator AnimationForPole()
    {
        for (int i = 0; i < fon.Length; i++)
        {
            AnimPole.sprite = fon[i];
            yield return new WaitForSeconds(changePictures);
        }

        
    }

    IEnumerator PenAudio()
    {
        for (int i = 0; i < 4; i++)
        {
            yield return new WaitForSeconds((penDraws + Time.deltaTime) / 4);
            pen.Play();
            yield return new WaitForSeconds((penDraws + Time.deltaTime) / 2);
        }
    }
}
