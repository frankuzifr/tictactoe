using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FireWork : MonoBehaviour
{
    public Image newFirework, winPanel;
    public Sprite[] fireworks;
    public Transform positionFire;
    public float changePictures;
    public GameObject[] oldFireworks;
    private void Start()
    {
            StartCoroutine(NewFirework());        
    }

    IEnumerator NewFirework()
    {
        positionFire.position = new Vector2(Random.Range(200, 900), Random.Range(300, 1300));
        for (int i = 0; i < fireworks.Length; i++)
        {
            newFirework.sprite = fireworks[i];
            yield return new WaitForSeconds(changePictures);
        }
        //������������� ������� ��� �������� ����������. ��-�� ������-�� ����, �������� ��� ������ �����, �� ���������� �������� ��������� ������������ �� �����, ��-�� ����
        //�������� ������������� ����� ������� �����. � ����� �� �������� � ������� ��� ��� � ��� �������
        Image newFire = Instantiate(newFirework, new Vector2(Random.Range(200, 900), Random.Range(300, 1300)), Quaternion.identity) as Image;
        newFire.color = new Color(Random.value, Random.value, Random.value);
        newFire.transform.SetParent(winPanel.transform);
        foreach (GameObject obj in oldFireworks)
        {
            Destroy(obj);
        }
    }
}
