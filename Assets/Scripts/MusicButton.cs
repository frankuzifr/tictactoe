using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class MusicButton : MonoBehaviour
{
    
    public AudioMixerGroup Mixer;
    public bool musicPlaying = true;
    public Sprite[] musicSprites;
    public Button musicButton;
    private SpriteState musicSpritePressed;
   
    private void Start()
    {
        if (PlayerPrefs.GetString("music") == "off")
        {
            Mixer.audioMixer.SetFloat("MasterVolume", -80);
            GetComponent<Image>().sprite = musicSprites[2];
            musicSpritePressed.pressedSprite = musicSprites[3];
            musicButton.spriteState = musicSpritePressed;
            musicPlaying = false;
        }
        else if(PlayerPrefs.GetString("music") == "on")
        {
            Mixer.audioMixer.SetFloat("MasterVolume", 0);
            GetComponent<Image>().sprite = musicSprites[0];
            musicSpritePressed.pressedSprite = musicSprites[1];
            musicButton.spriteState = musicSpritePressed;
            musicPlaying = true;
        }
    }

    public void Music()
    {

        if (musicPlaying)
        {
            Mixer.audioMixer.SetFloat("MasterVolume", -80);
            PlayerPrefs.SetString("music", "off");
            musicPlaying = false;
            GetComponent<Image>().sprite = musicSprites[2];
            musicSpritePressed.pressedSprite = musicSprites[3];
            musicButton.spriteState = musicSpritePressed;

        }
        else
        {
            Mixer.audioMixer.SetFloat("MasterVolume", 0);
            PlayerPrefs.SetString("music", "on");
            musicPlaying = true;
            GetComponent<Image>().sprite = musicSprites[0];
            musicSpritePressed.pressedSprite = musicSprites[1];
            musicButton.spriteState = musicSpritePressed;

        }
    }
    private void OnApplicationQuit()
    {
        PlayerPrefs.DeleteKey("music");
    }
}
