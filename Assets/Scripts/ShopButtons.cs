using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopButtons : MonoBehaviour
{
    public Button btnSelect;
    public Button[] selectedBtns;
    public Canvas[] canvasBtn;
    void Start()
    {
        if (PlayerPrefs.GetInt("selected") == 0)
        {
            selectedBtns[0].interactable = false;
            selectedBtns[1].interactable = true;
            canvasBtn[0].gameObject.SetActive(true);
            canvasBtn[1].gameObject.SetActive(false);
        }
        else if (PlayerPrefs.GetInt("selected") == 1)
        {
            selectedBtns[0].interactable = true;
            selectedBtns[1].interactable = false;
            canvasBtn[0].gameObject.SetActive(false);
            canvasBtn[1].gameObject.SetActive(true);
        }
    }

    public void ButtonsSettings()
    {
        if (btnSelect.tag == "select0")
        {
            PlayerPrefs.SetInt("selected", 0);
            selectedBtns[0].interactable = false;
            selectedBtns[1].interactable = true;
            canvasBtn[0].gameObject.SetActive(true);
            canvasBtn[1].gameObject.SetActive(false);

        }
        else if (btnSelect.tag == "select1")
        {
            PlayerPrefs.SetInt("selected", 1);
            selectedBtns[0].interactable = true;
            selectedBtns[1].interactable = false;
            canvasBtn[0].gameObject.SetActive(false);
            canvasBtn[1].gameObject.SetActive(true);
        }
    }
}