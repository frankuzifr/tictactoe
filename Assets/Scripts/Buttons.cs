﻿using UnityEngine;
using UnityEngine.SceneManagement;


public class Buttons : MonoBehaviour
{
    public void SinglePlayer()
    {
        SceneManager.LoadScene("GameSceneSP");
    }

    public void MultiPlayer()
    {
        SceneManager.LoadScene("GameSceneMP");
    }

    public void Back()
    {
        SceneManager.LoadScene("MenuScene");
    }


    public void Game()
    {
        SceneManager.LoadScene("GameSceneMP");
    }

    public void Shop()
    {
        SceneManager.LoadScene("ShopScene");
    }

    public void Restart()
    {
        
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }


   
}
