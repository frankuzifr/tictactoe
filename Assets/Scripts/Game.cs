﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Game : MonoBehaviour
{
    public Button cell;
    public Button[] cells;
    public Sprite[] ticTacToe;
    private int sim;
    private int tagBn;
    public GameObject win, restartButton, winEffects;
    public GameObject[] effectsToWin;
    public Sprite startImage;
    public Text winText, turnText;
    public AudioSource pen;

    private void Start()
    {
        PlayerPrefs.DeleteKey("sim");
        PlayerPrefs.DeleteKey("numOfClicks");
        for (int i = 1; i < 10; i++)
        {
            PlayerPrefs.DeleteKey("cell" + i);
        }
        sim = UnityEngine.Random.Range(1, 3);
        PlayerPrefs.SetInt("sim", sim);
        PlayerPrefs.SetInt("numOfClicks", 0);
        
        
        if (sim == 1)
        {
            turnText.text = "Player X turn";
        }
        else
        {
            turnText.text = "Player O turn";
        }
        StartCoroutine(Delay());
    }

    IEnumerator Delay()
    {
        foreach (Button but in cells)
        {
            but.interactable = false;
        }
        yield return new WaitForSeconds(1f);
        foreach (Button but in cells)
        {
            but.interactable = true;
        }
    }
    public void CellsClick()
    {
        PlayerPrefs.SetInt("numOfClicks", PlayerPrefs.GetInt("numOfClicks") + 1);
        sim = PlayerPrefs.GetInt("sim");
        tagBn = Convert.ToInt32(cell.tag);
        PlayerPrefs.SetInt("cell" + tagBn, sim);
        cell.image.sprite = ticTacToe[sim];
        cell.interactable = false;
        if (sim == 1)
        {
            PlayerPrefs.SetInt("sim", 2);
            turnText.text = "Player O turn";
        }
        else
        {
            PlayerPrefs.SetInt("sim", 1);
            turnText.text = "Player X turn";
        }
        pen.Play();
        if (PlayerPrefs.GetInt("numOfClicks") > 4)
        {
            Result();
        }
    }

    private void Result()
    {
        if ((PlayerPrefs.GetInt("cell" + 1) == 1 && PlayerPrefs.GetInt("cell" + 2) == 1 && PlayerPrefs.GetInt("cell" + 3) == 1) ||
            (PlayerPrefs.GetInt("cell" + 1) == 1 && PlayerPrefs.GetInt("cell" + 5) == 1 && PlayerPrefs.GetInt("cell" + 9) == 1) ||
            (PlayerPrefs.GetInt("cell" + 1) == 1 && PlayerPrefs.GetInt("cell" + 4) == 1 && PlayerPrefs.GetInt("cell" + 7) == 1) ||
            (PlayerPrefs.GetInt("cell" + 2) == 1 && PlayerPrefs.GetInt("cell" + 5) == 1 && PlayerPrefs.GetInt("cell" + 8) == 1) ||
            (PlayerPrefs.GetInt("cell" + 3) == 1 && PlayerPrefs.GetInt("cell" + 6) == 1 && PlayerPrefs.GetInt("cell" + 9) == 1) ||
            (PlayerPrefs.GetInt("cell" + 4) == 1 && PlayerPrefs.GetInt("cell" + 5) == 1 && PlayerPrefs.GetInt("cell" + 6) == 1) ||
            (PlayerPrefs.GetInt("cell" + 7) == 1 && PlayerPrefs.GetInt("cell" + 8) == 1 && PlayerPrefs.GetInt("cell" + 9) == 1) ||
            (PlayerPrefs.GetInt("cell" + 7) == 1 && PlayerPrefs.GetInt("cell" + 5) == 1 && PlayerPrefs.GetInt("cell" + 3) == 1))
        {
            turnText.gameObject.SetActive(false);
            win.SetActive(true);
            restartButton.SetActive(true);
            winText.text = "Player X WIN";

        }
        else if ((PlayerPrefs.GetInt("cell" + 1) == 2 && PlayerPrefs.GetInt("cell" + 2) == 2 && PlayerPrefs.GetInt("cell" + 3) == 2) ||
                 (PlayerPrefs.GetInt("cell" + 1) == 2 && PlayerPrefs.GetInt("cell" + 5) == 2 && PlayerPrefs.GetInt("cell" + 9) == 2) ||
                 (PlayerPrefs.GetInt("cell" + 1) == 2 && PlayerPrefs.GetInt("cell" + 4) == 2 && PlayerPrefs.GetInt("cell" + 7) == 2) ||
                 (PlayerPrefs.GetInt("cell" + 2) == 2 && PlayerPrefs.GetInt("cell" + 5) == 2 && PlayerPrefs.GetInt("cell" + 8) == 2) ||
                 (PlayerPrefs.GetInt("cell" + 3) == 2 && PlayerPrefs.GetInt("cell" + 6) == 2 && PlayerPrefs.GetInt("cell" + 9) == 2) ||
                 (PlayerPrefs.GetInt("cell" + 4) == 2 && PlayerPrefs.GetInt("cell" + 5) == 2 && PlayerPrefs.GetInt("cell" + 6) == 2) ||
                 (PlayerPrefs.GetInt("cell" + 7) == 2 && PlayerPrefs.GetInt("cell" + 8) == 2 && PlayerPrefs.GetInt("cell" + 9) == 2) ||
                 (PlayerPrefs.GetInt("cell" + 7) == 2 && PlayerPrefs.GetInt("cell" + 5) == 2 && PlayerPrefs.GetInt("cell" + 3) == 2))
        {
            turnText.gameObject.SetActive(false);
            win.SetActive(true);
            restartButton.SetActive(true);
            winText.text = "Player O WIN";

        }
        else if (PlayerPrefs.GetInt("numOfClicks") == 9)
        {
            turnText.gameObject.SetActive(false);
            win.SetActive(true);
            restartButton.SetActive(true);
            winText.text = "Nobody won";
            winEffects.SetActive(false);
            foreach (GameObject obj in effectsToWin)
            {
                obj.SetActive(false);
            }
        }
    }
}
